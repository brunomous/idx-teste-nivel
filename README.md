# Teste de Nível - Dev Front End [ React Native ] - Index Digital

## Instruções

### Execução do Projeto


**Para execução em plataformas Linux ou MacOS, utilizar o seguinte comando, de acordo com o package manager desejado:**

```
npm build-unix
```

ou

```
yarn build-unix
```


**Em plataforma Windows, utilizar o seguinte comando, de acordo com o package manager desejado:**

```
npm build-windows
```

ou

```
yarn build-windows
```


> Projeto construído sobre a plataforma Android. Há possibilidade de erros na execução em ambiente iOS.