import React, { Component } from "react";
import { View, ScrollView, TouchableOpacity, Text } from "react-native";
import { connect } from "react-redux";
import PostCard from "../components/PostCard";
import { styles } from "../styles/index";
import DrawerHeader from "../components/DrawerHeader";

class FavoritesView extends Component {
  listFavorites = (favs, navigation) => {
    if (favs && favs.length !== 0) {
      return (
        <ScrollView showsVerticalScrollIndicator={false} style={styles.content}>
          {favs.map((favoriteArray, key) => (
            <TouchableOpacity
              key={key}
              onPress={() => navigation.navigate("Post")}
            >
              <PostCard post={favoriteArray[1]} />
            </TouchableOpacity>
          ))}
        </ScrollView>
      );
    } else {
      return (
        <View style={styles.emptyFavoritesContainer}>
          <Text style={styles.emptyFavoritesText}>
            Nenhum favorito na sua lista.
          </Text>
        </View>
      );
    }
  };

  render() {
    let { favorites, navigation } = this.props;
    return (
      <View style={styles.container}>
        <DrawerHeader navigation={navigation} title={"Favorites"} />
        {this.listFavorites(favorites, navigation)}
      </View>
    );
  }
}

mapStateToProps = state => {
  return {
    favorites: state.feed.favorites
  };
};

mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FavoritesView);
