import React from "react";
import {
  View,
  Text,
  Image,
  ImageBackground,
  ScrollView,
  TouchableOpacity
} from "react-native";
import { Header, Left, Right } from "native-base";
import { setFavorite } from "../actions/post";
import { styles } from "../styles/index";
import { connect } from "react-redux";

class PostView extends React.Component {
  render() {
    let { post, favorites, setFavorite } = this.props;
    let category = post[0];
    let postInfo = post[1];
    return (
      <View style={styles.container}>
        <ImageBackground
          style={styles.headerImageBackground}
          source={require("../../assets/001.jpg")}
        >
          <Header
            style={styles.postViewHeader}
            noShadow
            androidStatusBarColor={"transparent"}
            iosBarStyle={"dark-content"}
          >
            <Left>
              <View style={styles.headerIconContainer}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image
                    source={require("../../assets/xxxhdpi/ic_back.png")}
                    style={styles.headerBackIcon}
                  />
                </TouchableOpacity>
              </View>
            </Left>
            <Right>
              <View style={styles.headerIconContainer}>
                <TouchableOpacity
                  onPress={() => {
                    setFavorite(post, favorites);
                  }}
                >
                  <Image
                    source={require("../../assets/xxxhdpi/ic_star.png")}
                    style={styles.headerFavoriteIcon}
                  />
                </TouchableOpacity>
              </View>
            </Right>
          </Header>
        </ImageBackground>
        <View style={{ flex: 1 }}>
          <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={styles.postScrollView}
          >
            <Text style={styles.title}>{postInfo.title.toUpperCase()}</Text>
            <Text style={styles.category}>{category.toLowerCase()}</Text>
            <Text style={styles.description}>{postInfo.description}</Text>
          </ScrollView>
        </View>
      </View>
    );
  }
}

mapStateToProps = state => {
  return {
    post: state.post.info,
    favorites: state.feed.favorites
  };
};

mapDispatchToProps = dispatch => {
  return {
    setFavorite: (post, favorites) => dispatch(setFavorite(post, favorites))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PostView);
