import React, { Component } from "react";
import {
  View,
  Text,
  ActivityIndicator,
  TouchableOpacity,
  ScrollView
} from "react-native";
import { connect } from "react-redux";
import { requestPosts } from "../actions/feed";
import { setPostInfo } from "../actions/post";
import PostCard from "../components/PostCard";
import DrawerHeader from "../components/DrawerHeader";
import { styles } from "../styles/index";

class HomeView extends Component {
  componentDidMount = () => {
    this.props.requestPosts();
  };

  showLoader = () => {
    return (
      <View style={styles.loader}>
        <ActivityIndicator size="large" color="#ff0090" />
      </View>
    );
  };

  listPosts = posts => {
    if (posts && posts.length !== 0) {
      return (
        <ScrollView showsVerticalScrollIndicator={false} style={styles.content}>
          {posts.map((section, key) => (
            <View key={key}>
              <View style={styles.categoryTitleContainer}>
                <Text style={styles.categoryTitle}>
                  {section.category.toLowerCase()}
                </Text>
              </View>
              {section.items.map((post, key) => (
                <TouchableOpacity
                  key={key}
                  onPress={() => this.viewPost([section.category, post])}
                >
                  <PostCard post={post} />
                </TouchableOpacity>
              ))}
            </View>
          ))}
        </ScrollView>
      );
    } else {
      return (
        <View style={styles.emptyFavoritesContainer}>
          <Text style={styles.emptyFavoritesText}>Nenhum post disponível.</Text>
        </View>
      );
    }
  };

  viewPost = post => {
    let { navigation, setPostInfo } = this.props;
    setPostInfo(post);
    navigation.navigate("Post");
  };

  render() {
    let { posts, navigation, loading, error } = this.props;
    return (
      <View style={styles.container}>
        <DrawerHeader navigation={navigation} title={"Home"} />
        {loading ? this.showLoader() : this.listPosts(posts)}
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    posts: state.feed.posts,
    loading: state.feed.loading,
    error: state.feed.error
  };
};

const mapDispatchToProps = dispatch => {
  return {
    requestPosts: () => requestPosts(dispatch),
    setPostInfo: post => dispatch(setPostInfo(post))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeView);
