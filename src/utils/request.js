import axios from "axios";

const urlPosts =
  "https://idx.digital/static/idx-teste-de-nivel/mobile-test-one.json";

export const fetchPosts = () => {
  return axios
    .get(urlPosts)
    .then(res => {
      return res.data;
    })
    .catch(error => {
      return error;
    });
};
