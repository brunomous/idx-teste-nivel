import { SET_POST_INFO, SET_FAVORITE, REMOVE_FAVORITE } from "./types";

export const setPostInfo = post => {
  return { type: SET_POST_INFO, info: post };
};

export const setFavorite = (post, favorites) => {
  if (favorites.length !== 0) {
    alreadyFavorite = favorites.filter(elem => {
      return elem === post;
    });
    return alreadyFavorite.length === 0
      ? { type: SET_FAVORITE, post: post }
      : { type: REMOVE_FAVORITE, post: post };
  } else {
    return { type: SET_FAVORITE, post: post };
  }
};
