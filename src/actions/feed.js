import { GET_POSTS, GET_POSTS_SUCCESS, GET_POSTS_FAIL } from "./types";
import { fetchPosts } from "../utils/request";

export const setPosts = posts => {
  return { type: GET_POSTS_SUCCESS, payload: { data: posts } };
};

export const requestPosts = dispatch => {
  dispatch({ type: GET_POSTS });
  return fetchPosts()
    .then(res => {
      res = Array.isArray(res) ? res : []
      dispatch(setPosts(res));
    })
    .catch(error => {
      dispatch({ type: GET_POSTS_FAIL });
    });
};
