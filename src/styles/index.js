import { StyleSheet } from "react-native";

export const colors = {
  primary: "#FF0090",
  white: "#FFFFFF"
};

export const fonts = {
  thin: "Montserrat-Thin",
  regular: "Montserrat-Regular",
  semibold: "Montserrat-SemiBold",
  light: "Montserrat-Light"
};

export const styles = StyleSheet.create({
  container: { flex: 1 },
  header: {
    backgroundColor: colors.white
  },
  headerIcon: {
    width: 24,
    height: 24
  },
  headerIconContainer: {
    paddingHorizontal: 10
  },
  headerTitle: {
    fontFamily: fonts.thin,
    fontSize: 20,
    color: colors.primary,
    marginBottom: 7
  },
  loader: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  content: {},
  categoryTitleContainer: {
    backgroundColor: colors.primary,
    padding: 4,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  categoryTitle: {
    fontFamily: fonts.regular,
    color: colors.white,
    marginLeft: 13,
    marginBottom: 3
  },
  emptyFavoritesContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 20
  },
  emptyFavoritesText: {
    fontFamily: fonts.thin,
    fontSize: 20
  },
  card: {
    flex: 1
  },
  cardTitle: {
    fontFamily: fonts.regular,
    fontSize: 13,
    color: colors.white
  },
  cardDescription: {
    fontFamily: fonts.thin,
    fontSize: 13,
    color: colors.white
  },
  textContainer: {
    flex: 1,
    justifyContent: "flex-end",
    paddingLeft: 15,
    paddingBottom: 10,
    backgroundColor: "rgba(0, 0, 0, 0.7)"
  },
  controlButtons: {
    width: 16,
    height: 16
  },
  imageSlider: {
    width: "100%",
    height: 180,
    borderTopWidth: 0.5,
    borderTopColor: colors.white,
    borderBottomWidth: 0.3,
    borderBottomColor: colors.white
  },
  postViewHeader: {
    backgroundColor: "transparent"
  },
  headerImageBackground: {
    width: "100%",
    height: 210
  },
  headerFavoriteIcon: {
    width: 24,
    height: 24
  },
  headerBackIcon: {
    width: 30,
    height: 18
  },
  title: {
    fontFamily: fonts.regular,
    fontWeight: "bold",
    fontSize: 16
  },
  category: {
    fontFamily: fonts.thin,
    color: colors.primary,
    fontSize: 12
  },
  description: {
    fontFamily: fonts.thin,
    paddingTop: 20
  },
  postScrollView: {
    flexGrow: 1,
    paddingHorizontal: 18,
    paddingVertical: 12,
    alignSelf: "center"
  },
  idxTextView: {
    backgroundColor: colors.primary,
    height: 130,
    alignItems: "flex-start"
  },
  idxTextBackground: {
    flex: 1,
    width: "100%",
    height: 130
  },
  idxTextContainer: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "rgba(0, 0, 0, 0.3)",
    alignItems: "flex-end"
  },
  idxText: {
    flex: 1,
    color: colors.white,
    fontSize: 20,
    fontFamily: fonts.semibold,
    marginLeft: 17,
    marginBottom: 15
  },
  drawerItemLabel: {
    fontFamily: fonts.thin,
    fontWeight: "normal",
    fontSize: 20
  }
});
