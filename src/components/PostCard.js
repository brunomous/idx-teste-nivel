import React, { Component } from "react";
import { View, Text, Image, ImageBackground } from "react-native";
import Swiper from "react-native-swiper";
import { styles } from "../styles/index";

export default class PostCard extends Component {
  render() {
    let { post } = this.props;
    let { galery } = post;
    return (
      <Swiper
        width={"100%"}
        height={180}
        showsButtons
        showsPagination={false}
        prevButton={
          <Image
            style={[styles.controlButtons, { marginLeft: 5 }]}
            source={require("../../assets/xxxhdpi/arrow_left.png")}
          />
        }
        nextButton={
          <Image
            style={[styles.controlButtons, { marginRight: 5 }]}
            source={require("../../assets/xxxhdpi/arrow_right.png")}
          />
        }
      >
        {galery.map((el, key) => (
          <View key={key} style={styles.card}>
            <ImageBackground
              source={{ uri: el.toString() }}
              style={styles.imageSlider}
            >
              <View style={styles.textContainer}>
                <Text style={styles.cardTitle}>{post.title.toUpperCase()}</Text>
                <Text style={styles.cardDescription}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </Text>
              </View>
            </ImageBackground>
          </View>
        ))}
      </Swiper>
    );
  }
}
