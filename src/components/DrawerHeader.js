import React, { Component } from "react";
import { styles } from "../styles/index";
import { View, Image, TouchableOpacity } from "react-native";
import { Header, Left, Body, Title } from "native-base";

export default class DrawerHeader extends Component {
  render() {
    let { navigation, title } = this.props;
    return (
      <Header
        style={styles.header}
        androidStatusBarColor={"transparent"}
        iosBarStyle={"dark-content"}
      >
        <Left>
          <View style={styles.headerIconContainer}>
            <TouchableOpacity onPress={() => navigation.openDrawer()}>
              <Image
                source={require("../../assets/xxxhdpi/ic_menu.png")}
                style={styles.headerIcon}
              />
            </TouchableOpacity>
          </View>
        </Left>
        <Body>
          <Title style={styles.headerTitle}>{title}</Title>
        </Body>
      </Header>
    );
  }
}
