import {
  GET_POSTS,
  GET_POSTS_SUCCESS,
  GET_POSTS_FAIL,
  SET_FAVORITE,
  REMOVE_FAVORITE
} from "../actions/types";
import { Alert } from "react-native";

const initialState = {
  posts: [],
  favorites: [],
  loading: false,
  error: ""
};

export default (feedReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_POSTS:
      return { ...state, loading: true };
    case GET_POSTS_SUCCESS:
      return { ...state, loading: false, posts: action.payload.data };
    case GET_POSTS_FAIL:
      return {
        ...state,
        loading: false,
        error: "Erro no carregamento dos posts!"
      };
    case SET_FAVORITE:
      Alert.alert(
        "",
        "Item adicionado aos favoritos!",
        [{ text: "OK", onPress: () => {} }],
        { cancelable: false }
      );
      return { ...state, favorites: [...state.favorites, action.post] };
    case REMOVE_FAVORITE:
      Alert.alert(
        "",
        "Item removido dos favoritos!",
        [{ text: "OK", onPress: () => {} }],
        { cancelable: false }
      );
      return {
        ...state,
        favorites: state.favorites.filter(item => item !== action.post)
      };
    default:
      return state;
  }
});
