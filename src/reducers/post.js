import { SET_POST_INFO } from "../actions/types";

const initialState = {
  info: {}
};

export default (postReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_POST_INFO:
      return { ...state, info: action.info };
    default:
      return state;
  }
});
