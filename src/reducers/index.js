import { createStore, combineReducers } from "redux";
import feedReducer from "./feed";
import postReducer from "./post";

const rootReducer = combineReducers({
  feed: feedReducer,
  post: postReducer
});

const configureStore = () => {
  return createStore(rootReducer);
};

export default configureStore;
