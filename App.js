import React from "react";
import { Text, View, ImageBackground } from "react-native";
import HomeView from "./src/containers/HomeView";
import FavoritesView from "./src/containers/FavoritesView";
import PostView from "./src/containers/PostView";
import {
  createDrawerNavigator,
  createStackNavigator,
  createAppContainer,
  DrawerItems
} from "react-navigation";
import { styles, colors } from "./src/styles/index";

const DrawerContent = props => (
  <View>
    <View style={styles.idxTextView}>
      <ImageBackground
        source={require("./assets/index.png")}
        style={styles.idxTextBackground}
      >
        <View style={styles.idxTextContainer}>
          <Text style={styles.idxText}>IDX</Text>
        </View>
      </ImageBackground>
    </View>
    <DrawerItems
      {...props}
      labelStyle={styles.drawerItemLabel}
      activeBackgroundColor="transparent"
      activeTintColor={colors.primary}
    />
  </View>
);

const DrawerNavigator = createDrawerNavigator(
  {
    Home: {
      screen: HomeView,
      navigationOptions: {
        drawerLabel: "Home"
      }
    },
    Favorites: {
      screen: FavoritesView,
      navigationOptions: {
        drawerLabel: "Favorites"
      }
    }
  },
  {
    contentComponent: DrawerContent
  }
);

const StackNavigator = createStackNavigator(
  {
    Drawer: {
      screen: DrawerNavigator
    },
    Post: {
      screen: PostView
    }
  },
  {
    headerMode: "none",
    navigationOptions: {
      headerVisible: false
    }
  }
);

export default createAppContainer(StackNavigator);
