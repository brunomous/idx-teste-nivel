/** @format */

import React, { Component } from "react";
import { AppRegistry } from "react-native";
import App from "./App";
import { name as appName } from "./app.json";
import "./ReactotronConfig";
import { Provider } from "react-redux";
import configureStore from "./src/reducers/index";

const store = configureStore();

export default (ReduxProvider = () => (
  <Provider store={store}>
    <App />
  </Provider>
));

AppRegistry.registerComponent(appName, () => ReduxProvider);
