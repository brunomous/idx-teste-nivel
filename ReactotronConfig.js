import Reactotron from "reactotron-react-native";

Reactotron
  .configure({ host: "192.168.25.52" })
  .useReactNative()
  .connect();
